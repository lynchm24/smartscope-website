from flask import Flask, render_template, request, redirect, url_for
app = Flask(__name__, static_folder='images')

# homepage, whysmartscope, products, contact, about

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/whysmartscope")
def whysmartscope():
    return render_template("whysmartscope.html")

@app.route("/products")
def products():
    return render_template("products.html")

@app.route("/contact")
def contact():
    return render_template("contact.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/login", methods = ["POST", "GET"])
def login():
    if request.method == "POST":
        user = request.form["name"]
        return redirect(url_for("user", usr = user))
    else:
        return render_template("login.html")


@app.route("/<usr>")
def user(usr):
    return f"<h1>{usr}</h1>"


if __name__ == "__main__":
    app.debug = True
    app.run()

# here I have 3 pages









# every time i want to use a package, i run into issue with having multiple python version
# so let's just get rid of all of the other version now

# i need to have the same version on my:

# 1. VScode interpreter 
# 2. Terminal
# 3. Virtual enviorment

# I will use version 3.11.1 for everything
# I need to bind this to python